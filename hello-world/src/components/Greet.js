import React from 'react'


const Hello = (props) => {
    return (
        <div>
            <h1>Hi, {props.name} is a {props.job}</h1>
            {props.children}
        </div>
    )
}

export default Hello



