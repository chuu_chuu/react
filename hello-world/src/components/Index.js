import React from 'react'


// function Greet() {
//     return <h1>Hello React</h1>
// }

const Greet = props => {
        return ( 
            <div>
                <h1>{props.name} is a {props.job}</h1>
                {props.children}
            </div>
        )
   
    
    
}
 
 


export default Greet