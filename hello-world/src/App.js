import React from 'react';
import logo from './logo.svg';


import Welcome from './components/Welcome'
import './App.css';

function App() {
  return (
    <div className="App">
     <Welcome />
    </div>
  );
}

export default App;
